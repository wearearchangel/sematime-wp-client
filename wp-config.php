<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'sematime');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '&Ff8;2h3J&N18 Ylwtx6w|1hW|C]~NKFT1i(8woanIgc>Z/{I*Fm,}zh]^>)+7<!');
define('SECURE_AUTH_KEY',  'NN!I.1=<IR);#)|TP#H`|fj*GtzRG1RU vC2t}rhe?cE.<8~Q+VgPw33<Qfs+.X$');
define('LOGGED_IN_KEY',    'm%eY>a6t0Rid+@M.1SrhScb)eRn{R)-%iAwayjtMrBaQ2]v.QB.397*=AHUAu!Rx');
define('NONCE_KEY',        'ravJn&#~@eI|m)#XC8vi+[2*Qp6qLX|Q>h.||kblKfN^_hCXhgRQqh1cDh1J}-<%');
define('AUTH_SALT',        'dXupi~.FuWg<b_GOfo.}&`_IeUm6+/lB[h*]g{2zX].,{-Td>Rj6is{!>P_wo35M');
define('SECURE_AUTH_SALT', '?FM.^B>)4-hJA356f~7~-fPY[H,?}os>b)fd1l@+O,bCKw>`2CH6`!H?Cd;w^azB');
define('LOGGED_IN_SALT',   'q8<r=:[+EX].yOdF-C}7pXroUtX_2*F.LW9=nY6{LHpZ-YIMENVtrL:^pd-liad:');
define('NONCE_SALT',       'JTD`9VWo 4qmQ-k%n-!!2xqbs>$q5[O7CHU.+Fea|YS{Zxp[+h{efNNh+7=m?,<Q');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'sm_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
